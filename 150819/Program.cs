﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _150819
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] esimene = { 1, 2, 3 };
            int[] teine = { 2, 7, 8 };
            esimene = esimene.Union(teine).ToArray(); //lisab arvud, mis erinevad

            Console.WriteLine(esimene.GetLength(0)); // peab lisama mõõtme

            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Console.WriteLine(arvud.Length);

            for (int i = 0; i < arvud.Length; i++)  //täidetakse mitu korda, tullakse tagasi kuni tingimus on täidetud. ALusta 0-st ja pane väiksem, kui tingimus. Seda muutujat väljaspool ei ole. 
            {
                Console.WriteLine(arvud[i]); //alustab trükkimist asukohaga 0 ja iga tsükliga liidetakse üks juurde kuni i on 8
            }

            // 1. initsiaator
            // 2. kontroll terminaator, kui false siis lõpetakse. kui true siis täidetakse täidetav blokk
            // 3. täidetakse iteraator
            // 4. mine tagasi sinna kus oli 2. samm

            // Neli For-tsükli võimalust:

            for (int k = 0; k < arvud.Length; k++) //for ja vajuta 2xTab => automaatselt tulevad tsükli asjad 
            {
                Console.WriteLine($"arv number {k} on {arvud[k]}"); //$ asendab otse placeholderid
            }

            for (int i2 = 0; i2 < arvud.Length;)
            {
                Console.WriteLine($"arv number {i2} on {arvud[i2++]}");
            }

            int i4 = 0;
            for (; i4 < arvud.Length;)
            {
                Console.WriteLine($"arv number {i4} on {arvud[i4++]}");
            }

            int b = 0;
            for (; ; ) // kaks ; peab alati olema
            {
                Console.WriteLine($"arv number {b} on {arvud[b++]}");
                if (b >= arvud.Length) break;
            }

            int[] teisedarvud = { 7, 2, 3, 8, 8, 9 };
            int g = 0;
            for (; g < arvud.Length; g++)
            {
                if (arvud[g] == 8) Console.WriteLine("kaheksa leidsin pesast {0}", g);

            }

            int[] kolmedarvud = { 7, 2, 3, 8, 8, 9 };

            for (int c = arvud.Length; c > 0;)
            {
                Console.WriteLine(kolmedarvud[--c]);
            }

            foreach (int x in arvud) //võta kõik arvud ja trüki välja: foreach arv x in arvud print out
            {
                Console.WriteLine(x);
            }
            foreach (var x in arvud)
            {
                Console.WriteLine(x);
            }


            int[] arvud2 = { 3, 3, 3, 4, 5, 6, 7, 8, 9 };

            foreach (var x in arvud2)
            {
                Console.WriteLine(x);
            }
            int arv = 7;
            Console.WriteLine(arv.GetType().Name); //Int32

            var nimi = 7; //saab kasutaja muutujate defineerimisel (me kirjutame väärtuse ja ta ise vaatab, mis ta on aga peab ütlema ka väärtuse)
            Console.WriteLine(nimi.GetType().Name);

            // var kasutakse kui on raske või võimatu öelda muutuja tüüp

            var henn = new { Nimi = "Henn Sarv", Vanus = 64 };
            Console.WriteLine(henn.GetType().Name);

            foreach (var x in arvud.GetType().GetProperties()) // saab küsida, mis omadused massiivil on
                Console.WriteLine(x.Name);

            while (true) // päises ainult üks avaldis, täidetakse kuni on true
            {
                Console.WriteLine("osta elevant ära ");
                var vastus = Console.ReadLine();
                if (vastus == "ostan") break;
                Console.Write($"Kõik ütlevad, et {vastus}, aga "); //ei tee reavahetust
            }
            Console.WriteLine("tubli, elefant on nüüd sinu");

            //võib ka:

            Console.Write("Tead mis, ");
            string vastus = "";
            while (vastus != "ostan") // kontrollitakse tingimust alguses
            {
                Console.Write("osta elevant ära ");
                vastus = Console.ReadLine();
                if (vastus == "ostan") break;
                Console.Write($"Kõik ütlevad, et {vastus}, aga "); // ilma if-ta prindib ka selle osa
            }
            Console.WriteLine("tubli, elefant on nüüd sinu");

            Console.Write("Tead mis, ");
            string vastus = "";
            do  // kontrollitakse tingimust lõpus
            {
                Console.Write("osta elevant ära ");
                vastus = Console.ReadLine();
                if (vastus == "ostan") break;
                Console.Write($"Kõik ütlevad, et {vastus}, aga "); // ilma if-ta prindib ka selle osa
            } while (vastus != "ostan"); // siin kontrollitakse. kirjutatakse bloki lõppu

            Console.WriteLine("tubli, elefant on nüüd sinu");

            string valgus = "";

            while (valgus != "roheline") // select area => Ctrl +SK => Surround with...
            {
                Console.Write("Mis tuli põleb fooris? ");
                valgus = Console.ReadLine();
                valgus = valgus.ToLower();
                if (valgus == "punane" | valgus == "kollane")
                    Console.WriteLine("Oota");
                else
                   if (valgus != "roheline")
                    Console.WriteLine("Osta prillid");
            }

            Console.WriteLine("Sõida!");

            // võib teha ka boolean muutuja ja täida tsükklit kuni boolean on true =>bool oota = true; while (oota) ja kuhugi kuhu vaja oota = false;

            const int ÕPILASTE_ARV= 1;
            string[] nimed = new string[ÕPILASTE_ARV];
            int[] vanused = new int[ÕPILASTE_ARV];

            int õpilasteArv = 0;
            for (; õpilasteArv < ÕPILASTE_ARV; õpilasteArv++) 
            {
                Console.Write($"Anna õpilase {nimed[õpilasteArv]} nimi: ");
                String nimi = Console.ReadLine();
                if (nimi == "") break;
                nimed[õpilasteArv] = nimi; //lisab Array asukohaga i uue stringi
                Console.WriteLine($"Anna õpilase {nimed[õpilasteArv]} vanus: ");
                int vanus = int.Parse(Console.ReadLine()); // see teisendab loetud arvu (string) arvuks (int)
                vanused[õpilasteArv] = vanus;
            }
            Console.WriteLine($"Õpilasi on {õpilasteArv}");

            double vanusteSumma = 0;
            for (int i = 0; i < õpilasteArv; i++) vanusteSumma +=  vanused[i];
            double keskminevanus = vanusteSumma / õpilasteArv;
            
            Console.WriteLine($"Keskmine õpilaste vanus on: {keskminevanus:F2}"); //:F2 annab kaks komakohta

            int vanimVanus = vanused[0];
            int kesOnVanim = 0;

            for (int i = 1; i < õpilasteArv; i++)
                if (vanused[i] > vanimVanus)
                    vanimVanus = vanused[kesOnVanim = i];

            Console.WriteLine($"vanim on {nimed[kesOnVanim]}");

            int kesOnRohkemKeskmine = 0;
            double algnevahe = keskminevanus - vanused[0];
            if (algnevahe < 0)
                algnevahe = algnevahe * -1;

            for (int i = 0; i < õpilasteArv; i++)
            {
                double vahe = keskminevanus - vanused[i];
                if (vahe < 0)
                    vahe = vahe * -1;

                if (vahe < algnevahe)
                    kesOnRohkemKeskmine = i;

            }
            Console.WriteLine($"rohkem keskmine on {nimed[kesOnRohkemKeskmine]}");

            int kl = 77;
            double d = Math.PI;
            DateTime täna = DateTime.Today;

            Console.WriteLine("Meie k on " + kl.ToString());
            Console.WriteLine("Meie d on " + d.ToString());
            Console.WriteLine("meie d on {0:F4}", d);
            Console.WriteLine(täna.ToString("(ddd) dd.MMMM yyyy"));
            Console.WriteLine($"Täna on {täna:dddd}");

            int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };
            int[] ruudud = new int[10];

            List<int> arvudelist = new List<int> { 1, 2, 3, 4, 5, 6, 7 };

            arvudelist.Add(14);
            arvudelist.Remove(4);
            arvudelist.Remove(6);
            arvudelist.RemoveAt(1);

            foreach (var x in arvud) Console.WriteLine(x);
            foreach (var x in arvudelist) Console.WriteLine(x);

            arvudelist.AddRange(Enumerable.Range(800, 1000)); // lisab mul listi arvud 800 .. 1799 (vahemiku)

            Console.WriteLine(arvudelist.Count); // caount ütleb palju meid on
            Console.WriteLine(arvudelist.Capacity);  //palju me ruumi võtame

            SortedSet<int> sorditud = new SortedSet<int> { 1, 7, 2, 8, 3, 1, 14, 2, 14 }; // vaid unikaalsed ja sortitud väärtused
            foreach (var x in sorditud) Console.WriteLine(x);

            var teine = arvudelist.ToArray();

            Dictionary<string, int> nimekiri = new Dictionary<string, int>(); // võti ja väärtus

            nimekiri.Add("Henn", 64);
            nimekiri.Add("Ants", 28);
            nimekiri.Add("Peeter", 40);

            Console.WriteLine(nimekiri["Henn"]);
            nimekiri["Ants"] = 128;
            nimekiri["Kalle"] = 33;

            foreach (var x in nimekiri) Console.WriteLine(x); // ka .Value

            Dictionary<string, string> sõnad = new Dictionary<string, string>()
            {
                {"love", "armastus" },
                {"hate", "viha" },
                {"money", "raha" },
                {"christmastree", "jõulupuu" },
            };

            Console.WriteLine(nimekiri.Values.Average());

            if (!nimekiri.ContainsKey("Kalle")) nimekiri.Add("Kalle", 33); // saab kontrollida ka nimekirjas on võti juba olemas, kui ei ole add Kalle 33


            Random r = new Random();
            SortedSet<int> juhuslikud = new SortedSet<int>();
            for (int i = 0; i < 1000; i++)
            {
                juhuslikud.Add(r.Next(100000)); // annab juhusliku arvu vahemikus 0...9999999
            }
            var juhuslikudlist = juhuslikud.ToList();

            foreach (var x in juhuslikud) Console.WriteLine(x);
            int mitu = juhuslikud.Count;
            if (mitu % 2 == 0)
                Console.WriteLine($"mediaan on {(juhuslikudlist[mitu/2] + juhuslikudlist[mitu/2-1]) / 2 }");
            else
                Console.WriteLine($"mediaan on {juhuslikudlist[mitu/2]}");


        }
    }
}
